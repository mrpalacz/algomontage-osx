//
//  AppDelegate.swift
//  Algomontage
//
//  Created by Julian on 08.01.20.
//  Copyright © 2020 Julian Palacz. All rights reserved.
//

import Cocoa
import AVFoundation

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    var colorSelector = ColorSelectController()
    let database = AGDatabase()
    let videoWindow = VideoWindow()
    let interfaceWindow = InterfaceWindow()
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        videoWindow.makeKeyAndOrderFront(nil)
        interfaceWindow.makeKeyAndOrderFront(nil)
        
        colorSelector.showWindow(self)
        
        let execColor =  {(c: ColorRange) in
            Logger.debug("searchExecuted")
            self.colorSelector.color!.color = c.color
            self.interfaceWindow.setColor(c.color)
            let composition = self.database.queryColor(c)!
            let item = AVPlayerItem(asset: composition)
            self.videoWindow.playItem(item)
        }
        
        interfaceWindow.searchExecuted = execColor
        colorSelector.searchExecuted = execColor
        
    }
}

