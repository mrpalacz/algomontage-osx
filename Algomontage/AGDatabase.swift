//
//  Database.swift
//  Algomontage
//
//  Created by Julian on 08.01.20.
//  Copyright © 2020 Julian Palacz. All rights reserved.
//

import Cocoa
import Foundation
import SQLite


struct AGDatabase {
    var db : Connection!
    
    let files = Table("files")
    let id = Expression<Int64>("id")
    let name = Expression<String>("name")
    let size = Expression<String>("size")
    
    let dirs = Table("dirs")
    let dir = Expression<String>("dir")
    
    let colors = Table("colors")
    let file_id = Expression<Int64>("file_id")
    let start = Expression<Double>("start")
    let end = Expression<Double>("end")
    let start_frame = Expression<Int64>("start_frame")
    let end_frame = Expression<Int64>("end_frame")
    let hue = Expression<Double>("h")
    let sat = Expression<Double>("s")
    let bri = Expression<Double>("b")
    let dominance = Expression<Double>("dominance")
    
    let filemanager = FileManager.default
    
    var directories = [URL]()
    init() {
        do {
            db = try Connection("/Users/julian/algomontage.sqlite")
            directories =  Array(try db.prepare(dirs)).map { row in
                return URL(fileURLWithPath: row[dir], isDirectory: true)
            }
            print(directories)
        } catch {
            print("Creating DB failed")
        }
    }
    
    func queryColor(_ range: ColorRange) -> AGMutableComposition? {
    
        let composition = AGMutableComposition()
        
        let query = files.join(colors, on: file_id == files[id]).where(
            hue >= Double(range.hueMin) && hue <= Double(range.hueMax) &&
            sat >= Double(range.saturationMin) && sat <= Double(range.saturationMax) &&
            bri >= Double(range.brightnessMin) && bri <= Double(range.brightnessMax)).order(dominance.desc).limit(5)
        do {
            let query = try db.prepare(query)
            Logger.debug(query)
            for color in query {
                guard let url = fileExists(inDirectories: directories, name: color[name]) else {continue}
                composition.append(url, start: color[start], end: color[end])
            }
        } catch {
            return nil
        }
        return composition
    }
}



func fileExists(inDirectories directories: [URL], name: String) -> URL? {
    for dir in directories {
        let url = dir.appendingPathComponent(name)
        if FileManager.default.fileExists(atPath: url.path) {
            return url
        }
    }
    Logger.warn("File \(name) could not be found!")
    return nil
}
