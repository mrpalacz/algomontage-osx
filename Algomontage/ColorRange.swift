//
//  NSColorRange.swift
//  Algomontage
//
//  Created by Julian on 16.01.20.
//  Copyright © 2020 Julian Palacz. All rights reserved.
//

import Cocoa

class ColorRange: NSObject {
    var color: NSColor
    var hueMargin: CGFloat
    var saturationMargin: CGFloat
    var brightnessMargin:CGFloat
    
    init(withColor color: NSColor, hueMargin: CGFloat, saturationMargin: CGFloat, brightnessMargin: CGFloat) {
        self.color = color
        self.hueMargin = hueMargin
        self.saturationMargin = saturationMargin
        self.brightnessMargin = brightnessMargin
    }
    
    convenience init(withColor color:NSColor) {
        let hueMargin = CGFloat(UserDefaults.standard.float(forKey: "Hue Margin"))
        let saturationMargin = CGFloat(UserDefaults.standard.float(forKey: "Saturation Margin"))
        let brightnessMargin = CGFloat(UserDefaults.standard.float(forKey: "Brightness Margin"))
        self.init(withColor: color, hueMargin: hueMargin, saturationMargin: saturationMargin, brightnessMargin: brightnessMargin)
    }
    
    var hueMin: CGFloat { get { return max(color.hueComponent - hueMargin/2, 0) } }
    var hueMax: CGFloat { get {return min(color.hueComponent + hueMargin/2, 1) } }
    var saturationMin: CGFloat { get { return max(color.saturationComponent - saturationMargin/2, 0) } }
    var saturationMax: CGFloat { get {return min(color.saturationComponent + saturationMargin/2, 1) } }
    var brightnessMin: CGFloat { get { return max(color.brightnessComponent - brightnessMargin/2, 0) } }
    var brightnessMax: CGFloat { get {return min(color.brightnessComponent + brightnessMargin/2, 1) } }

    var hueMinColor: NSColor { get {return NSColor(hue: hueMin, saturation: color.saturationComponent, brightness: color.brightnessComponent, alpha: 1.0) } }
    var hueMaxColor: NSColor { get {return NSColor(hue: hueMax, saturation: color.saturationComponent, brightness: color.brightnessComponent, alpha: 1.0) } }
    var saturationMinColor: NSColor { get {return NSColor(hue: color.hueComponent, saturation: saturationMin, brightness: color.brightnessComponent, alpha: 1.0) } }
    var saturationMaxColor: NSColor { get {return NSColor(hue: color.hueComponent, saturation: saturationMax, brightness: color.brightnessComponent, alpha: 1.0) } }
    var brightnessMinColor: NSColor { get {return NSColor(hue: color.hueComponent, saturation: color.saturationComponent, brightness: brightnessMin, alpha: 1.0) } }
    var brightnessMaxColor: NSColor { get {return NSColor(hue: color.hueComponent, saturation: color.saturationComponent, brightness: color.brightnessComponent, alpha: 1.0) } }

}
