//
//  Logger.swift
//  Algomontage
//
//  Created by Julian on 12.01.20.
//  Copyright © 2020 Julian Palacz. All rights reserved.
//

import Foundation

struct Logger {
    
    static let tagName = "Algomontage"
    
    static func print(_ items: Any..., separator: String = " ", level: String = "INFO", function: String = #function) {
        let message = items.map { "\($0)" }.joined(separator: separator)
        NSLog("\(level) [\(function)] \(message)")
    }
    
    static func debug(_ items: Any..., separator: String = " ", function: String = #function) {
        #if DEBUG
        self.print(items, separator: separator, level: "DEBUG", function: function)
        #endif
    }
    static func info(_ items: Any..., separator: String = " ", function: String = #function) {
        #if DEBUG
        self.print(items, separator: separator, level: "INFO", function: function)
        #endif
    }
    static func warn(_ items: Any..., separator: String = " ", function: String = #function) {
        self.print(items, separator: separator, level: "WARN", function: function)
    }
    
    static func error(_ items: Any..., separator: String = " ", function: String = #function) {
        self.print(items, separator: separator, level: "ERROR", function: function)
    }
}
