//
//  ColorSelectController.swift
//  Algomontage
//
//  Created by Julian on 15.01.20.
//  Copyright © 2020 Julian Palacz. All rights reserved.
//

import Cocoa


class ColorSelectController: NSWindowController {
    @IBOutlet weak var color: NSColorWell?
    @IBOutlet weak var hueMinColor: NSColorWell?
    @IBOutlet weak var hueMaxColor: NSColorWell?
    @IBOutlet weak var saturationMinColor: NSColorWell?
    @IBOutlet weak var saturationMaxColor: NSColorWell?
    @IBOutlet weak var brightnessMinColor: NSColorWell?
    @IBOutlet weak var brightnessMaxColor: NSColorWell?
        
    var searchExecuted: ((ColorRange) -> Void)?
    
    override var windowNibName: String? {
        return "ColorSelectController"
    }
    
    override func windowDidLoad() {
        super.windowDidLoad()
        let defaults = [
            "Hue Margin": 0.0,
            "Saturation Margin": 0.0,
            "Brightness Margin": 0.0,
        ] as [String : Any]
        UserDefaults.standard.register(defaults: defaults)
        setColorDisplays()
    }

    @IBAction func didSetColor(_ sender: Any) {
        setColorDisplays()
    }
    
    @IBAction func didSetMargin(_ sender: Any) {
        setColorDisplays()
    }

    
    @IBAction func search(_ sender: Any) {
        Logger.debug("pressed search button")
        if(searchExecuted != nil) {
            searchExecuted!(colorRange)
        }
    }
    
    var colorRange : ColorRange {
        get {
            return ColorRange(withColor: color!.color)
        }
    }
    
    func setColorDisplays() {
        hueMinColor!.color = colorRange.hueMinColor
        hueMaxColor!.color = colorRange.hueMaxColor
        saturationMinColor!.color = colorRange.saturationMinColor
        saturationMaxColor!.color = colorRange.saturationMaxColor
        brightnessMinColor!.color = colorRange.brightnessMinColor
        brightnessMaxColor!.color = colorRange.brightnessMaxColor
    }
}
