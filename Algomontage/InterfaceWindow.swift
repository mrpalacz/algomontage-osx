//
//  WebController.swift
//  Algomontage
//
//  Created by Julian on 28.01.20.
//  Copyright © 2020 Julian Palacz. All rights reserved.
//

import Cocoa
import Foundation
import WebKit

class InterfaceWindow: NSWindow, WKScriptMessageHandler {
    let webView = WKWebView()
    var searchExecuted: ((ColorRange) -> Void)?
    init() {
        let screen = NSScreen.screens.first!
        super.init(contentRect: NSMakeRect(screen.frame.origin.x,
                    screen.frame.origin.y,
                    2048/2,
                    1536/2),
                   styleMask: [.closable, .titled],
        backing: .buffered,
        defer: false)
        self.isMovableByWindowBackground = true
        
        webView.frame = self.contentView!.bounds
        self.contentView!.addSubview(webView)
        let url = Bundle.main.url(forResource: "index", withExtension: "html", subdirectory: "interface")!
        webView.configuration.userContentController.add(self, name: "colors")
        webView.configuration.userContentController.add(self, name: "movements")
        webView.configuration.userContentController.add(self, name: "sound")
        webView.loadFileURL(url, allowingReadAccessTo: url)
        let request = URLRequest(url: url)
        webView.load(request)
        
    }
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
       if message.name == "colors" {
        let dict = message.body as! [String:CGFloat]
        
        //guard let h = CGFloat(dict["h"] as Float) else {return}
        print(dict)
        print(dict["h"]!/360.0, dict["s"]!/100.0, dict["v"]!/100.0)
        
        let color = NSColor(hue:dict["h"]!/360.0, saturation:dict["s"]!/100.0, brightness: dict["v"]!/100.0, alpha: 1.0)
        let colorRange = ColorRange(withColor: color)
        //Logger.debug(colorRange)
        if(searchExecuted != nil) {
            searchExecuted!(colorRange)
        }
                
       }
    }
    func setColor(_ color: NSColor) {
        let js = "setColor({h: \(Int(color.hueComponent*360.0)), s: \(Int(color.saturationComponent*100.0)), v: \(Int(color.brightnessComponent*100.0))});"
        webView.evaluateJavaScript(js, completionHandler: nil)
    }
}
