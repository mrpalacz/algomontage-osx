$(function () {
  $('.navigation li').click(function(event) {
    event.preventDefault();
    $('.tab, .navigation li').removeClass('active');
    $(event.target.dataset.target).addClass("active");
    $(this).addClass("active");


  });

  var nw_ang = $(".slider_nw_ang").roundSlider({
    sliderType: "range",
    handleShape: "round",
    width: 30,
    radius: "100",
    value: "180,270",
    animation: false,
    startAngle: 180,
    max: "360",
    change: function (args) {
      console.log(args);
    }
  });
  var ne_ang = $(".slider_ne_ang").roundSlider({
    sliderType: "range",
    handleShape: "round",
    width: 30,
    radius: "100",
    value: "270,359",
    animation: false,
    startAngle: 180,
    max: "360",
    change: function (args) {
      console.log(args);
    }
  });
  var se_ang = $(".slider_se_ang").roundSlider({
    sliderType: "range",
    handleShape: "round",
    width: 30,
    radius: "100",
    value: "0,90",
    animation: false,
    startAngle: 180,
    max: "360",
    change: function (args) {
      console.log(args);
    }
  });
  var sw_ang = $(".slider_sw_ang").roundSlider({
    sliderType: "range",
    handleShape: "round",
    width: 30,
    radius: "100",
    value: "90,180",
    animation: false,
    startAngle: 180,
    max: "360",
    change: function (args) {
      console.log(args);
    }
  });
});

var opts = {
  orientation: 'vertical',
  start: [0, 2],
  direction: 'rtl',
  range: {
    'min': [0],
    'max': [20]
  }
};

var ne_mag = noUiSlider.create(document.querySelector('.slider_nw_mag'), opts);
var nw_mag = noUiSlider.create(document.querySelector('.slider_ne_mag'), opts);
var se_mag = noUiSlider.create(document.querySelector('.slider_sw_mag'), opts);
var sw_mag = noUiSlider.create(document.querySelector('.slider_se_mag'), opts);


var p = window.getComputedStyle(document.body).getPropertyValue('padding-top');

var h = window.innerHeight - parseInt(p)*2;
var w = h - (38+30)*2
var colorPicker = new iro.ColorPicker(".colorWheel", {
  width: w,
  height: h,
  handleRadius: 8,
  handleUrl: null,
  handleOrigin: {y: 0, x: 0},
  color: "#fff",
  sliderMargin: 30,
  borderWidth: 1,
  padding: 8,
  handleRadius: 10,
  wheelLightness: true,
  wheelAngle: 270,
  wheelDirection: 'anticlockwise',
  layout: [
    {
      component: iro.ui.Wheel,
      options: {
      }
    },
    {
      component: iro.ui.Slider,
      options: {
      }
    },
    {
      component: iro.ui.Slider,
      options: {
        sliderType: 'saturation'
      }
    }
  ]
});

colorPicker.on('input:end', function(color) {
  if (window.webkit) {
    window.webkit.messageHandlers.colors.postMessage(this.color.hsv);
  }
})

var setColor = function(color) {
    console.log(color);
    colorPicker.color.hsv = color;
}
