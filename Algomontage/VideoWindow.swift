//
//  VideoWindow.swift
//  Algomontage
//
//  Created by Julian on 16.01.20.
//  Copyright © 2020 Julian Palacz. All rights reserved.
//

import Cocoa
import AVFoundation

class VideoWindow: NSWindow {
    var mainView: MainView?
    
    let playerLayer = AVPlayerLayer()
    
    init() {
        let screen = NSScreen.screens.last!
        super.init(contentRect: NSMakeRect(screen.frame.origin.x,
                    screen.frame.origin.y,
                    1920/2,
                    1080/2),
        styleMask: [.closable, .resizable],
        backing: .buffered,
        defer: false)
        self.isMovableByWindowBackground = true
        
        mainView = MainView(frame: self.contentView!.bounds)
        self.contentView!.addSubview(mainView!)

        playerLayer.backgroundColor = .black
        mainView!.layer!.addSublayer(playerLayer)
        playerLayer.frame = mainView!.layer!.bounds
    }
    
    func playItem(_ playerItem: AVPlayerItem) {
        let videoPlayer = AVPlayer(playerItem: playerItem)
        playerLayer.player = videoPlayer
        Logger.debug("PlayerItem Duration: \(playerItem.duration.seconds)") // Shows the duration of all tracks together
        Logger.debug("PlayerItem Tracks: \(playerItem.tracks.count)") // Shows same number of tracks as the VideoComposition Track count

        videoPlayer.play()
        
    }

}

class MainView: NSView {
    override init(frame frameRect: NSRect) {
        super.init(frame:frameRect);
        self.autoresizingMask = [.height, .width]
        self.wantsLayer = true
        
        guard let layer = self.layer else { return }
        layer.backgroundColor = NSColor.black.cgColor
        layer.autoresizingMask = [.layerWidthSizable, .layerHeightSizable]
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
    }
    override func mouseUp(with event: NSEvent) {
        //let appDelegate = NSApplication.shared.delegate as! AppDelegate
        if event.clickCount == 1 {
            //appDelegate.display?.isPlaying = !(appDelegate.display!.isPlaying)
        }
        if event.clickCount == 2 {
            //appDelegate.toggleScreen(self)
        }
    }
}
