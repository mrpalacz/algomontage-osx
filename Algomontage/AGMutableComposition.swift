//
//  Composition.swift
//  Algomontage
//
//  Created by Julian on 08.01.20.
//  Copyright © 2020 Julian Palacz. All rights reserved.
//

import Foundation
import AVFoundation

class AGMutableComposition: AVMutableComposition {
    var assetDict: [URL:AVAsset] = [:]
    var videoTrack : AVMutableCompositionTrack?
    var audioTrack : AVMutableCompositionTrack?
    var lastTime: CMTime = CMTime.zero
    
    override init() {
        super.init()
        videoTrack = addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
        audioTrack = addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: Int32(kCMPersistentTrackID_Invalid))
    }
    
    func append(_ url:URL, start startTime: Double, end endTime: Double) {
        guard FileManager.default.fileExists(atPath: url.path) else {print("file does not exist: \(url.path)"); return}
        guard startTime < endTime  else { print("Failed: startTime < endTime"); return }
        let start = CMTime(seconds: startTime, preferredTimescale: 10000)
        let duration = CMTime(seconds: endTime - startTime, preferredTimescale: 10000)
        
        if !assetDict.keys.contains(url) {
            assetDict[url] = AVAsset(url: url)
        }
        let asset = assetDict[url]!

        let total = CMTimeAdd(start, duration)
        guard asset.duration >= total else { print("Failed: time out of bounds: \(asset.duration) < \(total)"); return }
        
        
        do {
            try videoTrack?.insertTimeRange(CMTimeRangeMake(start: start, duration: duration),
                                                       of: asset.tracks(withMediaType: AVMediaType.video)[0] ,
                                                       at: lastTime)
        } catch {
            print("Failed to insert video track")
        }
        if !asset.tracks(withMediaType: AVMediaType.audio).isEmpty {
            do {
                try audioTrack?.insertTimeRange(CMTimeRangeMake(start: start, duration: duration),
                                                           of: asset.tracks(withMediaType: AVMediaType.audio)[0] ,
                at: lastTime)
            } catch {
                print("Failed to insert audio track")
            }
            lastTime = CMTimeAdd(lastTime, duration)
        }
    }
}
